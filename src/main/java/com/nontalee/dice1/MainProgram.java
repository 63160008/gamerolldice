/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.dice1;

import java.util.Scanner;

/**
 *
 * @author nonta
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Enter Name :");
        String name = kb.next();
        Player p1 = new Player(name);
        
        RollDice rd1 = new RollDice(p1);
        rd1.Run();
    }
}
