/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.dice1;

/**
 *
 * @author nonta
 */
public class Player {
    private String name;
    private int win;
    private int lose;
    private int draw;
    
    Player(String name){
        this.name = name;
        win = 0;
        lose = 0;
        draw = 0;    
    }
    public String getName(){
        return name;
    }
    public int getWin(){
        return win;
    }
    public int getLose(){
        return lose;
    }
    public int getDraw(){
        return draw;
    }
    void upWin(){
        win++;
    }
    void upLose(){
        lose++;
    }
    void upDraw(){
        draw++;
    }
    
    public String toString(){
        return "Name : "+getName()+" [score] {win : "+getWin()+" | Draw : "+getDraw()+" | lose : "+getLose()+"}";
    }
}
