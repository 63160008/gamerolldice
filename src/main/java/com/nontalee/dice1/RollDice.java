/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.dice1;

import java.util.Random;

/**
 *
 * @author nonta
 */
public class RollDice {

    private Random rand = new Random();
    private int[] dice = {1, 2, 3, 4, 5, 6};
    private Player p1;
    private Player p2;
    private int dicep1, dicep2;
    private Player winner;

    RollDice(Player p1) {
        this.p1 = p1;
        p2 = new Player("Bot");
    }

    void roll() {
        dicep1 = dice[rand.nextInt(6)];
        dicep2 = dice[rand.nextInt(6)];
    }

    void showDice() {
        System.out.println(dicep1);
        System.out.println(dicep2);
    }

    void checkWin() {
        if (dicep1 > dicep2) {
            p1.upWin();
            p2.upLose();
            winner = p1;
        } else if (dicep1 < dicep2) {
            p1.upLose();
            p2.upWin();
            winner = p2;
        } else {
            p1.upDraw();
            p2.upDraw();
            winner = null;
        }
    }

    
    void showWin(){
        if(winner!=null){
            System.out.println(winner.getName()+" Winner!!!");
        }else{
            System.out.println("Draw!!!");
        }
        System.out.println(p1.getName()+"["+dicep1+"]-["+dicep2+"]"+p2.getName());
    }
    void showInfo(){
        System.out.println(p1.toString());
        System.out.println(p2.toString());
    }
    void Run(){
        roll();
        checkWin();
        showWin();
        showInfo();
    }

}
